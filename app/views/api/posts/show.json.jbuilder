json.post do
  json.(@post, :id, :title, :content)
  json.image_url @post.image.url(:medium, timestamp: false)
  json.created_at @post.created_at.strftime('%Y-%m-%d %H:%M')

  json.comments_count @post.comments.count
  json.likes_count @post.likes.count

  json.user do
    user = @post.user

    json.id user.id
    json.avatar_url user.avatar.url(:medium, titmestamp: false)
    json.name user.name
  end
end