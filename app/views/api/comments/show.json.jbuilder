json.comment do
  json.(@comment, :id, :content, :parent_id)

  json.user do
    user = @comment.user

    json.id user.id
    json.avatar_url user.avatar.url(:thumb, timestamp: false)
    json.name user.name
  end
end
