def json_ltree_builder(json, post)
  json.(post, :content)

  json.user do
    user = post.user

    json.avatar_url user.avatar.url(:medium, titmestamp: false)
    json.name user.name
  end

  children = post.children

  unless children.empty?
    json.comments do
      json.array! children do |child|
        json_ltree_builder(json, child)
      end
    end
  end
end

json.comments do
  json.array! @comments do |post|
    json_ltree_builder(json, post)
  end
end

json.meta do
  json.total_count @comments.total_count
  json.count @comments.size
  json.page @comments.current_page
  json.per_page (params[:per_page] ? params[:per_page] : Comment.default_per_page)
end