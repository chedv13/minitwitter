json.user do
  json.(@user, :id, :name, :email)

  json.avatars do
    json.thumb @user.avatar.url(:thumb, timestamp: false)
    json.medium @user.avatar.url(:medium, timestamp: false)
  end
end
