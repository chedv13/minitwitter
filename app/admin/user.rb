ActiveAdmin.register User do
  filter :email, as: :string

  form do |f|
    f.inputs do
      f.input :email
      f.input :name
      f.input :password
      f.input :password_confirmation
    end

    f.actions
  end

  index do
    selectable_column
    column :email
    column :name
    actions
  end

  permit_params do
    [:email, :name, :password, :password_confirmation]
  end
end
