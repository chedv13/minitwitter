Twitter.provider('LikeProvider', function () {
    this.$get = ['$resource', function ($resource) {
        return $resource('/api/posts/:post_id/likes/:_id', {}, {
            update: {
                method: 'PUT'
            },
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }];
});
