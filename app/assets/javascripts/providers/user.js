Twitter.provider('UserProvider', function () {
    this.$get = ['$resource', function ($resource) {
        return $resource('/api/users/:_id', {}, {
            update: {
                method: 'PUT'
            },
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }];
});
