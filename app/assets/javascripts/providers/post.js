Twitter.provider('PostProvider', function () {
    this.$get = ['$resource', function ($resource) {
        return $resource('/api/posts/:_id', {}, {
            update: {
                method: 'PUT'
            },
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }];
});
