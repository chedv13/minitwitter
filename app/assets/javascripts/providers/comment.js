Twitter.provider('CommentProvider', function () {
    this.$get = ['$resource', function ($resource) {
        return $resource('/api/posts/:post_id/comments/:_id', {}, {
            update: {
                method: 'PUT'
            },
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }];
});
