Twitter.controller('PostCtrl', function ($scope, $modal, CommentProvider, Session) {
    $scope.openCommentsModal = function (card) {
        $scope.comments = CommentProvider.query({post_id: card.id}, function () {
            $modal.open({
                animation: true,
                templateUrl: 'templates/comments.html',
                controller: 'CommentsModalCtrl',
                size: 1,
                resolve: {
                    card: function () {
                        return card;
                    },
                    comments: function () {
                        return $scope.comments.comments;
                    }
                }
            });
        });
    };

    $scope.addComment = function (card) {
        var comment = new CommentProvider;

        console.log(card.comment);

        comment.user_id = Session.currentUser.id;
        comment.post_id = card.id;
        comment.content = card.comment;

        comment.$save({post_id: card.id});

        card.comments_count += 1;
    };
});