Twitter.controller('UsersCtrl', ['$scope', '$cookieStore', '$routeParams', '$rootScope', 'PostProvider', 'UserProvider',
    function ($scope, $cookieStore, $routeParams, $rootScope, PostProvider, UserProvider) {
        $scope.currentUser = $scope.$parent.currentUser;

        if ($routeParams.id && $scope.currentUser && $routeParams.id != $scope.currentUser.id) {
            $scope.userData = UserProvider.get({'_id': $routeParams.id}, function () {
                $scope.user = $scope.userData.user;
            });
        } else {
            $scope.user = $scope.$parent.currentUser;
        }

        $scope.posts = PostProvider.query({user_id: $routeParams.id ? $routeParams.id : $scope.currentUser.id}, function () {
            $scope.totalItems = $scope.posts.meta.total_count;
            $scope.currentPage = 1;
        });

        $scope.update = function () {
            UserProvider.update({_id: $routeParams.id}, {
                email: $scope.user.email,
                name: $scope.user.name,
                password: $scope.user.password,
                password_confirmation: $scope.user.password_confirmation
            }).$promise.then(function () {
                    $cookieStore.put('current_user', $scope.user);
                }, function (reason) {
                    alert('Failed: ' + reason);
                }, function (update) {
                    alert('Got notification: ' + update);
                });
        };

        $scope.follow = function () {
            alert('follow');
        }
    }]);