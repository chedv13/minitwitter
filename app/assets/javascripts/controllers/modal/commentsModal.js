Twitter.controller('CommentsModalCtrl', function ($scope, $modalInstance, CommentProvider, Session, card, comments) {
    $scope.comments = comments;

    $scope.addComment = function () {
        var comment = new CommentProvider;

        comment.user_id = Session.currentUser.id;
        comment.post_id = card.id;
        comment.content = $scope.comment;

        comment.$save({post_id: card.id}).then(
            function (data) {
                card.comments_count += 1;
                $scope.comments.push(data.comment);
            },

            function (err) {
                alert(err);
            }
        );
    };

    $scope.ok = function () {
        console.log($scope.postData);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
