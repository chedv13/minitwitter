Twitter.controller('CreatePostModalCtrl', function ($scope, $modalInstance, PostProvider, $http, Session, $rootScope) {
    $scope.postData = {};
    $scope.fd = new FormData();

    $scope.dropzoneConfig = {
        'options': {
            'url': function () {

            }
        },
        'eventHandlers': {
            "addedfile": function (file, xhr, formData) {
                $scope.fd.append('post[image]', file);
            },
            'sending': function (file, xhr, formData) {
            },
            'success': function (file, response) {
            }
        }
    };

    $scope.ok = function () {
        $scope.fd.append('post[user_id]', Session.currentUser.id)
        $scope.fd.append('post[title]', $scope.postData.title);
        $scope.fd.append('post[content]', $scope.postData.content);

        $http.post('/api/posts.json', $scope.fd, { // The update method of the users controller
            withCredentials: true,
            headers: {
                'Content-Type': undefined,
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
            transformRequest: angular.identity
        }).success(function (e) {
            $rootScope.parentScope.addPost('test');
            $rootScope.parentScope = undefined;

            $modalInstance.close();
        }).error(function (e) {
            alert('Не удалось добавить пост!');

            $modalInstance.close();
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});