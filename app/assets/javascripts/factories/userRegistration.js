angular.module('twitterApp').factory('UserRegistration', ['$http', '$modal', '$location', function ($http) {
    var UserRegistration = function (options) {
        angular.extend(this, options);
    };

    UserRegistration.prototype.create = function (user) {
        return $http.post('/api/registrations', {
            'user': user
        });
    };

    return UserRegistration;
}]);