angular.module('twitterApp').factory('UserSession', ['$http', '$cookieStore', function ($http, $cookieStore) {
    var UserSession = function (options) {
        angular.extend(this, options);
    };

    UserSession.prototype.create = function (user) {
        return $http.post('/api/sessions', {
            'user': user
        });
    };

    UserSession.prototype.showCurrentUser = function () {
        return $http.get('/users/' + $cookieStore.get('current_user').id + '.json');
    };

    UserSession.prototype.destroy = function () {
        return $http.delete('/api/sessions');
    };

    return UserSession;
}]);