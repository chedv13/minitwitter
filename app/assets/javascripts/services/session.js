angular.module('twitterApp').service('Session', ['$cookieStore', 'UserSession', 'UserRegistration', function ($cookieStore, UserSession, UserRegistration) {
    this.currentUser = $cookieStore.get('current_user');
    this.signedIn = !!$cookieStore.get('current_user');
    this.signedOut = !this.signedIn;
    this.userSession = new UserSession();
    this.userRegistration = new UserRegistration();
}]);