// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require turbolinks
//= require angular.min
//= require angular-route.min
//= require angular-resource.min
//= require angular-cookies
//= require angular-deckgrid
//= require ng-rails-csrf
//= require dropzone
//= require imagesloaded
//= require ui-bootstrap-tpls-0.13.1.min
//= require wookmark.custom
//= require_tree .
