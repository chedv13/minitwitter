module Api
  class UsersController < ApplicationController
    respond_to :json

    before_action :set_user

    def show
      # render json: { user: @user }
    end

    def update
      respond_to do |format|
        if @user.update(user_params)
          format.json { render :show, status: :ok, location: api_user_url(@user), content_type: 'application/json' }
        else
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end

    private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params[:user].permit(:email, :name, :password, :password_confirmation)
    end
  end
end