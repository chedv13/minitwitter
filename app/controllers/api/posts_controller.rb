module Api
  class PostsController < ApplicationController
    before_action :set_post, only: [:show, :update]

    def index
      ar_lambda = lambda { params[:user_id] ? Post.where(user_id: params[:user_id]) : Post }

      @posts = ar_lambda.call
                   .order('created_at DESC')
                   .page(params[:page] ? params[:page] : 1)
                   .per(params[:per_page] ? params[:per_page] : Post.default_per_page)
    end

    def create
      @post = Post.new(post_params)

      respond_to do |format|
        if @post.save!
          format.json { render :show, status: :created, location: api_post_url(@post) }
        else
          format.json { render json: @post.errors, status: :unprocessable_entity }
        end
      end
    end

    def show
    end

    def update
      respond_to do |format|
        if @post.update(post_params)
          format.json { render :show, status: :ok, location: @post }
        else
          format.json { render json: @post.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      @post.destroy

      respond_to do |format|
        format.json { head :no_content }
      end
    end

    private

    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
      params[:post].permit(:content, :title, :image, :user_id)
    end
  end
end
