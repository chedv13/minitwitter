module Api
  class CommentsController < ApplicationController
    before_action :set_comment, only: [:show]

    def index
      @comments = Comment.where(post_id: params[:post_id]).page(1)
    end

    def create
      @comment = Comment.new(comment_params)

      respond_to do |format|
        if @comment.save!
          format.json { render :show, status: :created, location: "/api/posts/#{params[:post_id]}/comments/#{@comment.id}" }
        else
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end

    def show
    end

    private

    def set_comment
      @comment = Comment.find(params[:id])
    end

    def comment_params
      params[:comment].permit(:content, :post_id, :user_id)
    end
  end
end
