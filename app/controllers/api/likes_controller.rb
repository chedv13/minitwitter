module Api
  class LikesController < ApplicationController
    def index
      @likes = Like.where(post_id: params[:post_id]).page(1)
    end
  end
end
