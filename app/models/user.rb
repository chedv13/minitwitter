class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :avatar,
                    styles: {
                        medium: '300x300>', thumb: '100x100>'
                    },
                    default_url: '/images/:style/missing.png'

  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :posts, dependent: :destroy
  # Писал в два часа ночи, голова уже не соображала ...
  has_and_belongs_to_many :followers, class_name: 'User', foreign_key: :following_id, association_foreign_key: :follower_id, join_table: 'followers_followings'
  has_and_belongs_to_many :followings, class_name: 'User', foreign_key: :follower_id, association_foreign_key: :following_id, join_table: 'followers_followings'

  validates_attachment :avatar, content_type: { content_type: %w(image/jpg image/jpeg image/png image/gif) }
end
