class Post < ActiveRecord::Base
  paginates_per 10

  has_attached_file :image,
                    styles: {
                        medium: '300x300>', thumb: '100x100>'
                    },
                    default_url: '/images/:style/missing.png'

  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy

  validates :content, presence: true
  validates :title, presence: true
  validates :user_id, presence: true

  validates_attachment :image, content_type: { content_type: %w(image/jpg image/jpeg image/png image/gif) }
end
