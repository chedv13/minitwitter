class Comment < ActiveRecord::Base
  has_closure_tree
  paginates_per 10

  belongs_to :post
  belongs_to :user

  validates :content, presence: true
  validates :post, presence: true
  validates :user_id, presence: true
end
