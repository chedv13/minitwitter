Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users

  root 'welcome#index'

  namespace :api, defaults: { format: :json } do
    resources :posts, only: [:create, :delete, :index, :update, :show] do
      resources :comments, only: [:create, :delete, :index, :update]
      resources :likes, only: [:index]
    end

    resources :users, only: [:update, :show] do
      resources :posts, only: :index
    end

    devise_scope :user do
      post 'registrations' => 'registrations#create', :as => 'register'
      post 'sessions' => 'sessions#create', :as => 'login'
      delete 'sessions' => 'sessions#destroy', :as => 'logout'
    end
  end
end
