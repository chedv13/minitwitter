class CreateFollowersFollowings < ActiveRecord::Migration
  def change
    create_table :followers_followings, id: false do |t|
      t.integer :follower_id, index: true
      t.integer :following_id, index: true
    end
  end
end
